# Symfony currency converter

## Description

This php Symfony app is designed to create a currency converter that is able to take one base currency, a target currency and a value to be converted. The result is the value in the target currency.

## Requirements

 -  Docker
 -  Code editor (VSCode, Sublime)
 -  Postman
 -  Git

## Installation

1. Clone this git repository with `git clone https://gitlab.com/exercises8/currency-converter.git currency_converter_simon_martinez/` or with ssh `git@gitlab.com:exercises8/currency-converter.git currency_converter_simon_martinez/` command.

2. Go to the folder by running `cd currency_converter_simon_martinez/` or where root folder of this repository is located.

3. From the root directory of the project, run `docker-compose build --pull --no-cache` to build the images.

4. Once built is complete, run `docker-compose up` to run the containers.

## Usage

### Usage with POSTMAN:

  1. Find the postman file in the root folder (file `CurrencyConversionPostman.json`) and import it into your local installation of Postman and identify the request `Make a conversion`.

    * NOTE: You probably will need to disable the `SSL certificate verification` option in the Postman main settings.

  2. Open the body of the request and make sure you have the following json

  ```json
{
  "sourceCurrency": "USD",
  "sourceValue": 100,
  "targetCurrency": "EUR"
}
  ```

  3. Hit `Send` button to get a response with a currency conversion represented in this way:

```json
{
    "@context": "/api/contexts/CurrencyConversion",
    "@id": "/api/currency_conversions/107",
    "@type": "CurrencyConversion",
    "sourceCurrency": "USD",
    "sourceValue": 100,
    "targetCurrency": "EUR",
    "conversionFactor": 0.9459,
    "conversionResult": 94.5895,
    "createdAt": "2022-06-28T01:43:57+00:00"
}

```

 4. Change the values to the desired currencies and amounts. It works with any currency using the corresponding symbol, here are some examples:

 * US Dollar: USD
 * Euro: EUR
 * Schweizer Franken: CHF
 * Britisch Pfund: GBP 
 * Japanese Yen: JPY
 * Canadian Dollar: CAD
 * Mexican Peso: MXN

 ## APIs Swagger specs

  * Navigate to the https://localhost/api in a web browser to access the apis documentation. (You might need to bypass browser warning of accessing a site with self signed ssl certificate)

## Stack:

  API Platform (https://api-platform.com/docs/distribution/):

    * Caddy
    * PHP Symfony 6.1 
    * Postgres

Author: Simon Martinez simonm64@gmail.com