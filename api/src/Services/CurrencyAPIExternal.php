<?php

namespace App\Services;

use App\Entity\CurrencyConversion;
use Symfony\Contracts\HttpClient\HttpClientInterface;

final class CurrencyAPIExternal implements CurrencyService
{
    protected $baseUrl;
    protected $client;
    protected $apiKey;

    public function __construct(string $apiKey, string $baseUrl, HttpClientInterface $client)
    {
      $this->baseUrl = $baseUrl;
      $this->client = $client;
      $this->apiKey = $apiKey;
    }

    /**
     *  Isolating simple logic in the service to arrange minimal data
     * 
     *  @param obj App\Entity\CurrencyConversion;
     *  @return array with conversion factor and conversion result
     */
    public function getConversionData(CurrencyConversion $conversion): ?array
    {
        $data = $this->fetchConversionData($conversion);

        // TODO Work with validations on edge cases of api response

        $filteredData = [ 
          'conversionRate' => $data['rates'][$conversion->targetCurrency]['rate'],
          'conversionResult' => $data['rates'][$conversion->targetCurrency]['rate_for_amount']
        ];

        return $filteredData;
  }

  /**
   *  Encapsulating the API call to the contracted service. Using simple HTTP get request
   * 
   *  @param obj App\Entity\CurrencyConversion;
   *  @return array with response content
   */
  private function fetchConversionData(CurrencyConversion $conversion): ?array
  {
      // TODO Add try-catch block to deal with different responses

      $response = $this->client->request(
        'GET',
        $this->baseUrl . '/?api_key='.$this->apiKey.'&from=.',
        [
          'query' => [
            'api_key' => $this->apiKey,
            'from' => $conversion->sourceCurrency,
            'to' => $conversion->targetCurrency,
            'amount' => $conversion->sourceValue, 
            'format' => 'json'
          ]
        ]
      );
      $content = json_decode((string) ($response->getContent()), true);

      return $content;
  }


}