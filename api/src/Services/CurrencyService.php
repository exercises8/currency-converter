<?php

namespace App\Services;

use App\Entity\CurrencyConversion;

interface CurrencyService
{
  public function getConversionData(CurrencyConversion $conversion): ?array;
}