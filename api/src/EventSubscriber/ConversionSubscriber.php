<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use ApiPlatform\Core\EventListener\EventPriorities;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\CurrencyConversion;
use App\Services\CurrencyService;

class ConversionSubscriber implements EventSubscriberInterface
{
    private $currencyService;

    public function __construct(CurrencyService $myCurrencyService ){

        $this->currencyService = $myCurrencyService;

    }

    public function onKernelView(ViewEvent $event)
    {
        // ...
    }

    /**
     * Using the events to add the conversion logic
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['calculateMyConversion', EventPriorities::PRE_WRITE],
        ];
    }

    /**
     * This function is designed to interface with the currencyService abstract layer
     * and handle the conversion data logic before inserting entity.
     * @obj $event
     */
    public function calculateMyConversion(ViewEvent $event): void
    {
        $method = $event->getRequest()->getMethod();
        $conversion = $event->getControllerResult();
        if (!$conversion instanceof CurrencyConversion || Request::METHOD_POST !== $method) {
            return;
        }
        // Logic to call the service
        $conversionData = $this->currencyService->getConversionData($conversion);

        // Set the conversion factor in entity
        $conversion->setConversionFactor($conversionData['conversionRate']);

        // Set the result in entity
        $conversion->setConversionResult($conversionData['conversionResult']);

        // TODO Implement unit test for logic functions
    }
}
