<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CurrencyConversionRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: CurrencyConversionRepository::class)]
#[ApiResource(
    collectionOperations: [
        'get',
        'post'
    ],
    itemOperations: ['get'],
    normalizationContext: [
        "groups" => ["conv","conv:read"],
    ],
    denormalizationContext: [
        "groups" => ["conv","conv:write"],
    ]
)]
class CurrencyConversion
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups("conv")]
    #[Assert\NotBlank(groups: ['conv'])]
    public $sourceCurrency;

    #[ORM\Column(type: 'float')]
    #[Groups("conv")]
    #[Assert\Type("float", groups: ['conv'])]
    #[Assert\GreaterThan(0, groups: ['conv'])]
    public $sourceValue = 0;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups("conv")]
    #[Assert\NotBlank(groups: ['conv'])]
    public $targetCurrency = '';

    #[ORM\Column(type: 'float')]
    #[Groups("conv:read")]
    private $conversionFactor;

    #[ORM\Column(type: 'float')]
    #[Groups("conv:read")]  
    private $conversionResult = 40.2;

    #[ORM\Column(type: 'datetime')]
    #[Groups("conv:read")]
    private $createdAt;

    public function __construct(){

        $this->createdAt = new \DateTime();

    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSourceCurrency(): ?string
    {
        return $this->sourceCurrency;
    }

    public function setSourceCurrency(string $sourceCurrency): self
    {
        $this->sourceCurrency = $sourceCurrency;

        return $this;
    }

    public function getSourceValue(): ?float
    {
        return $this->sourceValue;
    }

    public function setSourceValue(float $sourceValue): self
    {
        $this->sourceValue = $sourceValue;

        return $this;
    }

    public function getTargetCurrency(): ?string
    {
        return $this->targetCurrency;
    }

    public function setTargetCurrency(string $targetCurrency): self
    {
        $this->targetCurrency = $targetCurrency;

        return $this;
    }

    public function getConversionFactor(): ?float
    {
        return $this->conversionFactor;
    }

    public function setConversionFactor(float $conversionFactor): self
    {
        $this->conversionFactor = $conversionFactor;
        
        return $this;
    }

    public function getConversionResult(): ?float
    {
        return $this->conversionResult;
    }

    public function setConversionResult(float $conversionResult): self
    {
        $this->conversionResult = $conversionResult;

        return $this;
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

}
