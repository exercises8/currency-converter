<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CurrencyRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: CurrencyRepository::class)]
#[ApiResource(
    collectionOperations: [
        'post',
        'get'
    ],
    itemOperations: [
        'get',
        'put',
        'delete'
    ],
    normalizationContext: [
        "groups" => ["currency","currency:read"],
    ],
    denormalizationContext: [
        "groups" => ["currency","currency:write"],
    ]
)]
class Currency
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups("currency")]
    #[Assert\NotBlank(groups: ['currency'])]
    private $symbol;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups("currency")]
    #[Assert\NotBlank(groups: ['currency'])]
    private $name;

    #[ORM\Column(type: 'datetime')]
    #[Groups("currency:read")]
    private $createdAt;

    public function __construct(){

        $this->createdAt = new \DateTime();

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSymbol(): ?string
    {
        return $this->symbol;
    }

    public function setSymbol(string $symbol): self
    {
        $this->symbol = $symbol;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

}
